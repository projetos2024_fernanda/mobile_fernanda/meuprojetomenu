import { StatusBar } from "expo-status-bar";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet, Text, View } from "react-native";
import Menu from "./src/components/menu";
import TerceiraPagina from "./src/terceiraPagina";
import QuintaPagina from "./src/quintaPagina";
import SegundaPagina from "./src/segundaPagina";
import QuartaPagina from "./src/quartaPagina";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="TerceiraPagina" component={TerceiraPagina} />
        <Stack.Screen name="QuintaPagina" component={QuintaPagina} />
        <Stack.Screen name="SegundaPagina" component={SegundaPagina} />
        <Stack.Screen name="QuartaPagina" component={QuartaPagina} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
