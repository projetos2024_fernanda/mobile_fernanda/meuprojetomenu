import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useEffect, useState } from "react";

export default function SegundaPagina() {
  const [count, setCount] = useState(0);
  

  return (
    <View style={styles.container}>
      <Text>CLIQUE AQUI PARA CONTAR :{count}</Text>

      <TouchableOpacity>
        style={styles.teste}
        onPress={() => setCount(count + 1)}
        <Text>Clique!</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "pink",
    alignItems: "center",
    justifyContent: "center",
  },

  teste: {
    color: "black",
    backgroundColor: "red",
    width: 90,
    borderRadius: 25,
    alignItems: "center",
  },
});
