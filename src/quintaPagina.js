import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";


export default function QuintaPagina({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity 
      style={styles.menu}
      onPress={() => navigation.navigate("QuintaPagina")}
      >
        <Text>Botão</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "pink",
    alignItems: "center",
    justifyContent: "center",
  },
});
